/** Скроллинг - работа с навигацией **/

/** Секции **/
var sections = [
    {
        min: 0 * window.innerHeight,
        max: 1 * window.innerHeight,
        name: 'Имя секции 1'
    },

    {
        min: 1 * window.innerHeight,
        max: 2 * window.innerHeight,
        name: 'Имя секции 2'
    }
];


/** Слушатель события on scroll **/
window.addEventListener('scroll', function(event) {
    var scrolled = window.pageYOffset || document.documentElement.scrollTop;

    sections.forEach((result) => {
        
        if ((result.max >= scrolled) && (result.min <= scrolled)) {
            document.getElementById('name-section').textContent = result['name'];
            
        }

    });
});

/** Конец скроллинг - работа с навигацией **/

/** Работа с сообщениями **/
function visibleMessage() {
    var msg = document.getElementById('container-message');

    if (msg.dataset.visible == 'false') {
        msg.dataset.visible = true;
        msg.style.height = '50px';
    } else if (msg.dataset.visible == 'true') {
        msg.dataset.visible = false;
        msg.style.height = '0px';
    }

}
/** Конец работа с сообщениями **/